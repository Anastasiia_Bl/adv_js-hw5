class Card {
	constructor({ id, title, body, user: { name, email } }) {
		this.id = id;
		this.title = title;
		this.body = body;
		this.name = name;
		this.email = email;
		this.element = this.render();
		this.delete();
	}

	render() {
		const template = document.querySelector('#card-template');
		const card = template.content.cloneNode(true).querySelector('.card');
		card.querySelector('.card-title').textContent = this.title;
		card.querySelector('.card-text').textContent = this.body;
		card.querySelector('.card-author').textContent = this.name;
        card.querySelector('.card-author-email').textContent = this.email;
		card.dataset.id = this.id;
		return card;
	}

	delete() {
		this.element.querySelector('.delete-button').addEventListener('click', () => {

			if(this.element.dataset.id = 'new') {
				this.element.remove();
			} else {
				fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
				method: 'DELETE'
			})
			.then(response => {
				if (response.ok) {
					this.element.remove();
				}
			})
			.catch(error => console.log(error));
			}
		});
		
	}
}

function getUsers() {
	return fetch("https://ajax.test-danit.com/api/json/users")
		.then(response => response.json())
}

function getPosts() {
	return fetch('https://ajax.test-danit.com/api/json/posts')
		.then(response => response.json())
}

function renderCards(posts) {
	const cardsContainer = document.querySelector('#cards-container');
	posts.forEach(post => {
		const { id, title, body, user } = post;
		const card = new Card({ id, title, body, user });
		cardsContainer.appendChild(card.element);
	});
}


function addPromiseAll() {
	Promise.all([getUsers(), getPosts()])
	.then(([users, posts]) => {
		posts.forEach(post => {
			post.user = users.find(({id}) => id === post.userId);
		});
		if (posts && posts.length) {
			renderCards(posts);
			document.querySelector('.loader').remove(); // видалення спінеру завантаження
			document.querySelector('#cards-container').style.display = 'block'; 
		}
	})
	.catch(error => console.log(error));
}
addPromiseAll(); 


// Додавання постів
const addPostButton = document.querySelector('#add-post-button');
const addPostModal = document.querySelector('#add-post-modal');
const closeButton = document.querySelector('.close-button');
const createPostButton = document.querySelector('#create-post-button');


addPostButton.addEventListener('click', () => {
  addPostModal.style.display = 'block';
});

closeButton.addEventListener('click', () => {
  addPostModal.style.display = 'none';
});

// кнопка Опублікувати
document.querySelector('#create-post-button').addEventListener('click', async () => {
	const title = document.querySelector('#post-title-input').value;
	const body = document.querySelector('#post-body-input').value;
	const userId = 1;

	try {
	  const response = await fetch('https://ajax.test-danit.com/api/json/posts', {
		method: 'POST',
		body: JSON.stringify({ title, body, userId})
	  });
	  if (response.ok) {
		const newPost = await response.json();
		const users = await getUsers(); // отримуємо масив користувачів
		newPost.user = users.find(user => user.id === userId); // знаходимо користувача з id=1
		const card = new Card(newPost);
		const cardsContainer = document.querySelector('#cards-container');
        cardsContainer.prepend(card.element);
		card.element.dataset.id = 'new';
		// очистити поле вводу
		document.querySelector('#post-title-input').value = "";
		document.querySelector('#post-body-input').value = "";
    	} 
	}
    catch(error) {
		console.log(error);
	} 
});






















// Код без деструктуризації
// class Card {
// 	constructor(post) {
// 		this.post = post;
// 		this.element = this.render();
// 		this.delete();
		
// 	}

// 	render() {
// 		const template = document.querySelector('#card-template');
// 		const card = template.content.cloneNode(true).querySelector('.card');
// 		card.querySelector('.card-title').textContent = this.post.title;
// 		card.querySelector('.card-text').textContent = this.post.body;
// 		card.querySelector('.card-author').textContent = this.post.user.name;
//         card.querySelector('.card-author-email').textContent = this.post.user.email;
// 		card.dataset.id = this.post.id;
// 		return card;
// 	}

// 	delete() {
// 		this.element.querySelector('.delete-button').addEventListener('click', () => {

// 			if(this.element.dataset.id = 'new') {
// 				this.element.remove();
// 			} else {
// 				fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
// 				method: 'DELETE'
// 			})
// 			.then(response => {
// 				if (response.ok) {
// 					this.element.remove();
// 				}
// 			})
// 			.catch(error => console.log(error));
// 			}
// 		});
		
// 	}
// }

// function getUsers() {
// 	return fetch("https://ajax.test-danit.com/api/json/users")
// 		.then(response => response.json())
// }

// function getPosts() {
// 	return fetch('https://ajax.test-danit.com/api/json/posts')
// 		.then(response => response.json())
// }

// function renderCards(posts) {
// 	const cardsContainer = document.querySelector('#cards-container');
// 	posts.forEach(post => {
// 		const card = new Card(post);
// 		cardsContainer.appendChild(card.element);
// 	});
// }

// function addPromiseAll() {
// 	Promise.all([getUsers(), getPosts()])
// 	.then(([users, posts]) => {
// 		posts.forEach(post => {
// 			post.user = users.find(({id}) => id === post.userId);
// 		});
// 		if (posts && posts.length) {
// 			renderCards(posts);
// 			document.querySelector('.loader').remove(); // видалення спінеру завантаження
// 			document.querySelector('#cards-container').style.display = 'block'; 
// 		}
// 	})
// 	.catch(error => console.log(error));
// }
// addPromiseAll(); 


// // Додавання постів
// const addPostButton = document.querySelector('#add-post-button');
// const addPostModal = document.querySelector('#add-post-modal');
// const closeButton = document.querySelector('.close-button');
// const createPostButton = document.querySelector('#create-post-button');


// addPostButton.addEventListener('click', () => {
//   addPostModal.style.display = 'block';
// });

// closeButton.addEventListener('click', () => {
//   addPostModal.style.display = 'none';
// });

// // кнопка Опублікувати
// document.querySelector('#create-post-button').addEventListener('click', async () => {
// 	const title = document.querySelector('#post-title-input').value;
// 	const body = document.querySelector('#post-body-input').value;
// 	const userId = 1;

// 	try {
// 	  const response = await fetch('https://ajax.test-danit.com/api/json/posts', {
// 		method: 'POST',
// 		body: JSON.stringify({ title, body, userId})
// 	  });
// 	  if (response.ok) {
// 		const newPost = await response.json();
// 		const users = await getUsers(); // отримуємо масив користувачів
// 		newPost.user = users.find(user => user.id === userId); // знаходимо користувача з id=1
// 		const card = new Card(newPost);
// 		const cardsContainer = document.querySelector('#cards-container');
//         cardsContainer.prepend(card.element);
// 		card.element.dataset.id = 'new';
// 		// очистити поле вводу
// 		document.querySelector('#post-title-input').value = "";
// 		document.querySelector('#post-body-input').value = "";
//     	} 
// 	}
//     catch(error) {
// 		console.log(error);
// 	} 
// });
























// const postTitleInput = document.querySelector('#post-title-input');
// const postBodyInput = document.querySelector('#post-body-input');

// 	проміс без перевірки
// Promise.all([getUsers(), getPosts()])
// 	.then(([users, posts]) => {
// 		posts.forEach(post => {
// 			post.user = users.find(user => user.id === post.userId);
// 		});
// 		renderCards(posts);
// 	})
// 	.catch(error => console.log(error));


// function getPosts() {
// 	return fetch('https://ajax.test-danit.com/api/json/posts', {
//         method: 'GET'
//     })
// 		.then(response => response.json())
// 		.then(data => console.log(data));
// }
// getPosts()

// function getUsers() {
// 	return fetch("https://ajax.test-danit.com/api/json/users", {
//         method: 'GET'
// })
// 		.then(response => response.json())
// 		.then(data => console.log(data));
// }
// getUsers()



